package ua.dbuhaichuk.finaleproject.db;

import ua.dbuhaichuk.finaleproject.db.entity.UserCard;

/**
 * Role entity.
 *
 * @author D.Buhaichuk
 *
 */
public enum Role {
    ADMIN, ENROLLEE;

    public static Role getRole(UserCard user) {
        int roleId = user.getRoleId();
        return Role.values()[roleId];
    }

    public String getName() {
        return name().toLowerCase();
    }

}