package ua.dbuhaichuk.finaleproject.db.entity;

/**
 * User Card entity.
 *
 * @author D.Buhaichuk
 *
 */
public class UserCard extends Entity {

    private static final long serialVersionUID = -6854282256149495388L;

    private String login;

    private String password;

    private int roleId;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "User Card [login=" + login + ", password=" + password + ", roleId=" + roleId + "]";
    }

}