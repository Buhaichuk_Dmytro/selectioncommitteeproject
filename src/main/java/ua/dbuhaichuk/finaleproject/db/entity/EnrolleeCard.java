package ua.dbuhaichuk.finaleproject.db.entity;

/**
 * Enrollee Card entity.
 *
 * @author D.Buhaichuk
 *
 */
public class EnrolleeCard extends Entity {

    private static final long serialVersionUID = -4897456256149495388L;

    private int userId;

    private String firstName;

    private String fatherName;

    private String lastName;

    private String city;

    private int regionId;

    private String schoolName;

    // certificate

    private boolean isBlocked = false;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFatherName() { return fatherName; }

    public void setFatherName(String fatherName) { this.fatherName = fatherName; }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public boolean isBlocked() {return isBlocked;}

    public void doBlock() {this.isBlocked = true;}

    @Override
    public String toString() {
        return "Enrollee Card [firstName=" + firstName + ", fatherName=" + fatherName + ", lastName=" +
                lastName + ", city=" + city + ", regionId=" + regionId + ", schoolName=" + schoolName + "]";
    }

}
