package ua.dbuhaichuk.finaleproject.db.entity;

/**
 * Claim entity.
 *
 * @author D.Buhaichuk
 *
 */
public class Claim extends Entity{

    private static final long serialVersionUID = -5885036256149495388L;

    private int enrolleeId;

    private int departmentId;

    private int firstGrade;

    private int secondGrade;

    private int thirdGrade;

    private int priority;

    private boolean isApproved = false;

    public int getEnrolleeId() { return enrolleeId; }

    public void setEnrolleeId(int enrolleeId) { this.enrolleeId = enrolleeId; }

    public int getDepartmentId() { return departmentId; }

    public void setDepartmentId(int departmentId) { this.departmentId = departmentId; }

    public int getFirstGrade() { return firstGrade; }

    public void setFirstGrade(int firstGrade) {
        this.firstGrade = firstGrade;
    }

    public int getSecondGrade() {
        return secondGrade;
    }

    public void setSecondGrade(int secondGrade) { this.secondGrade = secondGrade; }

    public int getThirdGrade() { return thirdGrade; }

    public void setThirdGrade(int thirdGrade) { this.thirdGrade = thirdGrade; }

    public int getPriority() { return priority; }

    public void setPriority(int priority) { this.priority = priority; }

    public boolean isApproved() {return isApproved;}

    public void doApprove() {this.isApproved = true;}


    @Override
    public String toString() {
        return "Claim [enrolleeId=" + enrolleeId + ", departmentId=" + departmentId + ", firstGrade=" +
                firstGrade + ", secondGrade=" + secondGrade + ", thirdGrade=" + thirdGrade +
                ", priority=" + priority + ", isApproved=" + isApproved + "]";
    }

}
