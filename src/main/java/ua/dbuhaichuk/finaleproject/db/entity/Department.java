package ua.dbuhaichuk.finaleproject.db.entity;

/**
 * Department entity.
 *
 * @author D.Buhaichuk
 *
 */
public class Department extends Entity{

    private static final long serialVersionUID = -3258974587123698756L;

    private String name;

    private int placesTotal;

    private int placesBudget;

    private int firstSubjectId;

    private int secondSubjectId;

    private int thirdSubjectId;

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public int getPlacesTotal() { return placesTotal; }

    public void setPlacesTotal(int placesTotal) { this.placesTotal = placesTotal; }

    public int getPlacesBudget() { return placesBudget; }

    public void setPlacesBudget(int placesBudget) { this.placesBudget = placesBudget; }

    public int getFirstSubjectId() { return firstSubjectId; }

    public void setFirstSubjectId(int firstSubjectId) { this.firstSubjectId = firstSubjectId; }

    public int getSecondSubjectId() { return secondSubjectId; }

    public void setSecondSubjectId(int secondSubjectId) { this.secondSubjectId = secondSubjectId; }

    public int getThirdSubjectId() { return thirdSubjectId; }

    public void setThirdSubjectId(int thirdSubjectId) { this.thirdSubjectId = thirdSubjectId; }
}
