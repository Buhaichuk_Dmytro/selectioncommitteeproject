package ua.dbuhaichuk.finaleproject.db;

import ua.dbuhaichuk.finaleproject.db.entity.EnrolleeCard;

/**
 * Region entity.
 *
 * @author D.Buhaichuk
 *
 */
public enum Region {
    CHERKASY, CHERNIHIV, CHERNIVTSI, DNIPROPETROVSK, DONETSK, IVANO_FRANKIVSK, KHARKIV,
    KHERSON, KHMELNYTSKYI, KYIV, KIROVOHRAD, LUHANSK, LVIV, MYKOLAIV, ODESSA, POLTAVA,
    RIVNE, SUMY, TERNOPIL, VINNYTSIA, VOLYN, ZAKARPATTIA, ZAPORIZHIA, ZHYTOMYR;

    public static Region getRegion(EnrolleeCard enrollee) {
        int regionId = enrollee.getRegionId();
        return Region.values()[regionId];
    }

    public String getName() {
        return name().toLowerCase();
    }

}